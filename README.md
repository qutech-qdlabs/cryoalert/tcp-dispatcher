# TCP Dispatcher

The TCP Dispatcher is a simple application to allow for multiple connections to multiple remote connections that do or do not support multiple simultaneous connections. The result is that other applications can connect to multiple devices, using a variety of connections/protocols, as if there is only a single device and connection. This simplifies those applications. Currently it supports the following outgoing connections:

- [x] TCP
- [x] COM port
- [x] VISA
  - [x] Generic VISA connector
  - [x] GPIB  

The following commands are available:

```cmd
> poetry run tcp_dispatcher --help     
Usage: tcp_dispatcher [OPTIONS] COMMAND [ARGS]...

  CLI commands to run the TCP Dispatcher

Options:
  --help  Show this message and exit.

Commands:
  init   CLI command to init a folder with the correct structure to...
  start  CLI command to start the tcp dispatcher
```

Furthermore, an example configuration can be found in the tcp_dispatcher/config folder to be used as a reference.

## Message Structure

### Multiple connectors

The message structure is defined as follows:

```txt
@<connector_name>#<data>
```

The connector name refers to a connector configuration in the configuration file. In this configuration file, each connector will have a unique name, as shown below.

```toml
[[connectors]]
type            = "COM"
name            = "testCOM"
com_port        = "COM3"
baudrate        = 9600      # 115200 by default
bytesize        = 8      # 8 bits by default
parity          = "N"      # N = no parity (default), E = parity
stop_bits       = 1     # 1 stop bit by default
termination     = "CRLF" # Line termination CRLF, CR, LF
timeout         = 5       # timeout of serial connection
    
[[connectors]]
type            = "GPIB"
name            = "testGPIB"
device_number   = 12
board_number    = 0
```

When only a single connector is defined, the connector name prefix of the message is optional. A warning (and an empty response) will be generated when no connector name is prefixed when multiple connectors are defined in the configuration file.

### Multiple messages to single connector

Besides the ability to route messages through multiple connectors, it is also possible to send multiple messages to a single connector within a single message send to the tcp_dispatcher. This is possible by appending a new message to the end of the message string, seperated by a semi-colon (;). A typical message could thus look like:

```py
b'@testCOM#@5R1;@2R1;@1V\r'
```

The response will be similarly concatenated by semi-colons (;) and the line termination of the last response will be kept.

## Installation

First, create a folder in which the configuration and logfiles can live. Then, go into this folder in the terminal.

```cmd
mkdir example
cd example
```

It is prefered to run the tcp dispatcher in a virtual environment, as not to interfer with any other python installations on the machine.
This can be done using the following command (assuming python has been installed on the machine):

```cmd
python -m venv .venv
```

To work in the virtual environment, it needs to be activated.
This can be done using:

```cmd
.\.venv\Scripts\activate.bat
```

It is then possible to install the tcp dispatcher in the virtual environment:

```cmd
pip install tcp-dispatcher --extra-index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```

A default setup can then be generated using:

```cmd
tcp_dispatcher init
```

This will generate a config file `config.toml`. Adjust this as needed. Additionally, this wil generate a folder into which all log files will be stored, and a script to facilitate the start up of the tcp dispatcher.

## Development

For development, the following steps are needed:

- clone this repository
- install poetry globally: `pip install poetry`
- (Optional) configure poetry to create a virtual environment in the project folder such that your IDE knows which interpreter it needs to look at.
- install dependencies and create virtual environment using `poetry install`
- run tcp_dispatcher locally using `poetry run tcp_dispatcher <command>`
- New features should always be pushed to a new feature_branch before merging into main (or develop, if it exists)

## Publish

A helper script has been made to make publication easier. However, to use it, some settings needs to be configured to make poetry aware of where to publish it.

First, add the tudelft gitlab repository to the poetry config;

```cmd
> poetry config repositories.gitlab-tudelft https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi
```

Afterwards, add configurtation for accessing the repository. Using the below command will prompt you for a password.

```cmd
> poetry config http-basic.gitlab-tudelft <pip-access-token>
```

If thats done successfully, the following script can be run to publish the package to the registry:

```cmd
> .\.build-support\publish\publish.ps1
Building tcp_dispatcher (1.0.0)
  - Building sdist
  - Built tcp_dispatcher-1.0.0.tar.gz
  - Building wheel
  - Built tcp_dispatcher-1.0.0-py3-none-any.whl

Publishing tcp_dispatcher (1.0.0) to gitlab-tudelft
 - Uploading tcp_dispatcher-1.0.0-py3-none-any.whl 0%
 - Uploading tcp_dispatcher-1.0.0-py3-none-any.whl 100%
 - Uploading tcp_dispatcher-1.0.0.tar.gz 0%
 - Uploading tcp_dispatcher-1.0.0.tar.gz 100%
```
