from tcp_dispatcher.connectors.visa_client import VisaClient


class GPIBClient(VisaClient):
    def __init__(self, device_number: int, **kwargs) -> None:
        self.board_number = kwargs.pop("board_number", None)
        self.device_number = device_number

        super().__init__(address=self._construct_resource_address(), **kwargs)

    def _construct_resource_address(self) -> str:
        bus = f"GPIB{self.board_number}" if self.board_number is not None else "GPIB"
        return f"{bus}::{self.device_number}::INSTR"
