from abc import ABC, abstractmethod


class AbstractConnector(ABC):
    @abstractmethod
    def connect(self) -> None:
        ...

    @abstractmethod
    def disconnect(self) -> None:
        ...

    @abstractmethod
    def send(self, data: bytes) -> bytes:
        ...
