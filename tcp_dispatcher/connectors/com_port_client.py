import logging
from typing import Optional

import serial

from tcp_dispatcher.connectors.base import AbstractConnector
from tcp_dispatcher.utils.maps import TERMINATION_CHAR_MAP

LOGGER = logging.getLogger(__name__)


class ComPortClient(AbstractConnector):
    def __init__(
        self,
        com_port: str,
        baudrate: int,
        bytesize: int,
        parity: str,
        stop_bits: int,
        termination: str,
        timeout: int,
        **kwargs,
    ):
        self.com_port = com_port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stop_bits = stop_bits
        self.termination = TERMINATION_CHAR_MAP[termination]
        self.timeout = timeout

        self._connection: Optional[serial.Serial] = None
        self.is_connected = False

    def connect(self) -> None:
        if not self.is_connected:
            try:
                self._connection = serial.Serial(
                    port=self.com_port,
                    baudrate=self.baudrate,
                    bytesize=self.bytesize,
                    parity=self.parity,
                    stopbits=self.stop_bits,
                    timeout=self.timeout,
                )
                self.is_connected = True
            except serial.SerialException as exc:
                LOGGER.error(exc)
                self.disconnect()

    def disconnect(self) -> None:
        if self.is_connected:
            self._connection.close()

        self.is_connected = False

    def send(self, data: bytes) -> bytes:
        # Ensure connection has been made
        try:
            LOGGER.debug("Check connection")
            self.connect()
        except serial.SerialException:
            LOGGER.error("Failed to create connection")
            self.disconnect()
            return b"\r\n"

        # Write to connection
        try:
            LOGGER.debug(f"Sending: {data}")
            self._connection.write(data)
        except serial.SerialException:
            LOGGER.error("Failed to write data")
            self.disconnect()
            return b"\r\n"

        # Read response, always something is returned
        try:
            LOGGER.debug("Retrieve data")
            res = self._connection.read_until(expected=self.termination)
        except (serial.SerialException, serial.SerialTimeoutException):
            LOGGER.error("Failed to read data")
            self.disconnect()
            return b"\r\n"

        # Check if response is not empty, might indicate connection has been closed
        LOGGER.debug(f"Received: {res}")
        if not res:
            LOGGER.warning("Received empty data, perhaps connection is closed?")
            return b"\r\n"

        # If everything is OK
        return res
