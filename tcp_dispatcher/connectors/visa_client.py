import logging
from typing import Optional

import pyvisa as visa
from pyvisa.resources import MessageBasedResource

from tcp_dispatcher.connectors.base import AbstractConnector

LOGGER = logging.getLogger(__name__)


class VisaClient(AbstractConnector):
    def __init__(self, address: str, **kwargs) -> None:
        self.address = address
        if "name" in kwargs:
            self.name = kwargs.pop("name")

        if "type" in kwargs:
            self.type = kwargs.pop("type")

        self.resource_kwargs = kwargs

        self._rm = visa.ResourceManager()
        self._connection: Optional[MessageBasedResource] = None
        self.is_connected = False

    def connect(self) -> None:
        if not self.is_connected:
            self._connection = self._rm.open_resource(self.address)
            self.is_connected = True

            # Add attributes to opened resource
            for key, val in self.resource_kwargs.items():
                if not hasattr(self._connection, key):
                    LOGGER.warning(
                        f"Adding attribute '{key}' to {self.__class__.__name__} \
                        that it did not already have. This might be an configuration error"
                    )

                setattr(self._connection, key, val)

    def disconnect(self) -> None:
        if self.is_connected:
            self._connection.close()
            self._connection = None
            self.is_connected = False

    def send(self, data: bytes) -> bytes:
        # Ensure a connection has been made
        try:
            LOGGER.debug("Check Connection")
            self.connect()
        except visa.errors.VI_ERROR_TMO:
            LOGGER.error("Failed to create connection")
            self.disconnect()
            return b"\r\n"

        # Write and read (query) connection
        try:
            # message comes in as bytes from TCP; decode (using utf-8)
            msg = data.decode()
            res = self._connection.query(msg).encode()
        except:
            LOGGER.error("Failed to write or read data")
            self.disconnect()
            return b"\r\n"

        LOGGER.debug(f"Received: {res}")
        if not res:
            LOGGER.warning("Received empty data, perhaps connection is closed?")
            return b"\r\n"

        # If everything is OK
        return res
