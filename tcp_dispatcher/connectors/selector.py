from enum import Enum
from typing import Optional

from tcp_dispatcher.connectors.base import AbstractConnector
from tcp_dispatcher.connectors.com_port_client import ComPortClient
from tcp_dispatcher.connectors.gpib_client import GPIBClient
from tcp_dispatcher.connectors.tcp_client import TcpRemoteClient
from tcp_dispatcher.connectors.visa_client import VisaClient
from tcp_dispatcher.utils.options import ConnectorTypes


class ConnectorException(Exception):
    """
    Custom Exception for handling Connector Exceptions
    """


def get_connector_class(connector_type: Optional[str] = None) -> type[AbstractConnector]:
    """
    Function to select a connector class based on the connector_type argument
    """

    if connector_type is None:
        raise ConnectorException("A connector type must be selected!")

    if connector_type.upper() == ConnectorTypes.TCP.value.upper():
        return TcpRemoteClient
    elif connector_type.upper() == ConnectorTypes.COM.value.upper():
        return ComPortClient
    elif connector_type.upper() == ConnectorTypes.VISA.value.upper():
        return VisaClient
    elif connector_type.upper() == ConnectorTypes.GPIB.value.upper():
        return GPIBClient
    else:
        raise ConnectorException(f"Unknown connector_type {connector_type} has been selected!")
