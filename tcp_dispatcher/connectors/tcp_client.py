import logging
import socket
from typing import Optional

from tcp_dispatcher.connectors.base import AbstractConnector

LOGGER = logging.getLogger(__name__)


class TcpRemoteClient(AbstractConnector):
    def __init__(self, remote_host: str, remote_port: int, **kwargs) -> None:
        self._socket: Optional[socket.socket] = None
        self._host = remote_host
        self._port = remote_port
        self.is_connected = False

    def connect(self) -> None:
        if not self.is_connected:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect((self._host, self._port))
            self.is_connected = True

    def disconnect(self) -> None:
        if self.is_connected:
            self._socket.close()
            self.is_connected = False
            self._socket = None

    def send(self, data: bytes) -> bytes:
        # Check if is connected. If not, connect
        try:
            LOGGER.debug("Check connection")
            self.connect()

            LOGGER.debug(f"Sending: {data}")
            self._socket.sendall(data)

            LOGGER.debug("Retrieve data")
            res = self._socket.recv(2048)
        except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError):
            LOGGER.info(f"Could not connect to remote server located on {self._host, self._port}")
            self.disconnect()
            return b"\r\n"

        LOGGER.debug(f"Received: {res}")
        if not res:
            LOGGER.info(f"Connection on remote server located on {self._host, self._port} was closed")
            res = b"\r\n"
            self.disconnect()
        return res
