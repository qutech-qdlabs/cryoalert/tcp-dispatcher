import logging
import re
from typing import Any

from tcp_dispatcher.connectors.base import AbstractConnector
from tcp_dispatcher.connectors.selector import get_connector_class

LOGGER = logging.getLogger(__name__)


class Router:
    """
    Class for routing messages to the correct connector if multiple are provided from the config
    """

    MSG_REGEX = re.compile(r"^(@?(?P<address>\w+)#)?(?P<data>\S+)(?P<term>\s+)?$")

    def __init__(self, connectors_conf: list[dict[str, Any]]) -> None:
        if len(connectors_conf) == 0:
            LOGGER.warning("No connectors are provided in the configuration!")

        self.connectors_map: dict[str, AbstractConnector] = {
            conn_conf["name"]: get_connector_class(conn_conf["type"])(**conn_conf) for conn_conf in connectors_conf
        }

    @property
    def num_conns(self) -> int:
        return len(self.connectors_map)

    def send(self, msg: bytes) -> bytes:
        # Check if connectors are present. If not, simply return empty response
        if self.num_conns == 0:
            LOGGER.debug("No connectors are present, therefore no message will be send")
            return b"\r\n"

        # Decode message from bytes to str
        msg_str = msg.decode(encoding="utf-8")

        # Match the message. If no match is found, simply return empty response
        m = self.MSG_REGEX.match(msg_str)
        if m is None:
            LOGGER.warning(f"Could not extract an adress or data from to-be-send message!")
            return b"\r\n"

        # Extract address and data parts of the message
        address, data, term = m.group("address"), m.group("data"), m.group("term")

        # Check conditions for address
        if address is None and self.num_conns != 1:
            LOGGER.warning("No adress was provided, but multiple connectors are present! No message will be send")
            return b"\r\n"

        # Set default term
        if term is None:
            LOGGER.warning("No line termination was found, setting it to '\\r\\n'")
            term = "\r\n"

        # If no address was provided and only a single connector is provided,
        # Simply select that one
        if address is None and self.num_conns == 1:
            address = list(self.connectors_map.keys())[0]

        # Check if address is in the connectors_map
        if address not in self.connectors_map:
            LOGGER.error(f"'{address}' not found in the configured connections list")
            return b"\r\n"

        # Send message to addressed connector per message
        outb = b""
        conn = self.connectors_map[address]
        data_cmds = data.split(";")
        for idx, data_cmd in enumerate(data_cmds):
            if idx != 0:
                outb += b";"

            # Keep the line terminator of the last response
            if idx == len(data_cmds) - 1:
                outb += conn.send(bytes(data_cmd + term, encoding="utf-8"))
            else:
                # Strip the line_terminators from all but the last response
                outb_resp = conn.send(bytes(data_cmd + term, encoding="utf-8"))
                outb += outb_resp.decode().strip().encode(encoding="utf-8")

        # Return the concatenated result
        return outb

    def connect(self) -> None:
        for _, conn in self.connectors_map.items():
            conn.connect()

    def disconnect(self) -> None:
        for _, conn in self.connectors_map.items():
            conn.disconnect()
