import logging
import logging.handlers


def init_logging(log_level: str | int = logging.DEBUG):
    # create logger
    logger = logging.getLogger()
    logger.setLevel(log_level)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(log_level)

    # create formatter
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    # Add a timed rotating file handler to the logging
    trfh = logging.handlers.TimedRotatingFileHandler("logs/tcp_dispatcher.log", when="D", interval=1, backupCount=14)
    trfh.setFormatter(formatter)
    logger.addHandler(trfh)
