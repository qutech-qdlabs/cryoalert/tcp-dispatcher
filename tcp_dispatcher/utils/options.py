from enum import Enum


class ConnectorTypes(Enum):
    TCP = "TCP"
    COM = "COM"
    VISA = "VISA"
    GPIB = "GPIB"


connector_types = [ct.name for ct in ConnectorTypes]
