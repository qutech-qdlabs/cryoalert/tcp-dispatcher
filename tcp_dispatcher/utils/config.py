import collections.abc
import os
from copy import deepcopy
from pathlib import Path
from typing import Optional, TypedDict

import toml


class ComConfig(TypedDict):
    type: str
    com_port: str
    baudrate: int
    bytesize: int
    parity: str
    stop_bits: int
    timeout: int


class TcpConfig(TypedDict):
    remote_host: str
    remote_port: int


class DispatcherConfig(TypedDict):
    dispatcher_port: int


class Config(TypedDict):
    log_level: str
    dispatcher: DispatcherConfig
    connectors: list[ComConfig | TcpConfig]


DEFAULT_CONFIG: Config = {
    "log_level": "DEBUG",
    "dispatcher": {
        "dispatcher_port": 33577,
    },
}


def read_config(config_path: Optional[str] = None) -> Config:
    # Read and parse config
    if config_path is None:
        return DEFAULT_CONFIG
    else:
        full_config_path = Path(os.getcwd()) / Path(config_path)
        return update_dict(DEFAULT_CONFIG, toml.load(full_config_path))


def update_dict(default: dict, update: dict):
    """
    Function to recursively update a dictionary at varying nesting levels
    https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
    """
    # Make deepcopy of default dict for safety
    default_copy = deepcopy(default)

    for key, value in update.items():
        if isinstance(value, collections.abc.Mapping):
            default_copy[key] = update_dict(default_copy.get(key, {}), value)
        else:
            default_copy[key] = value

    return default_copy
