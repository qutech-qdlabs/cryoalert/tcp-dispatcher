import os
import shutil
from datetime import datetime
from pathlib import Path
from typing import Any, Optional

import click
import pyvisa as visa
import serial
from jinja2 import Environment, PackageLoader
from pyfiglet import Figlet
from serial.tools.list_ports import comports

from tcp_dispatcher.server import TcpDispatcher
from tcp_dispatcher.utils.config import read_config
from tcp_dispatcher.utils.logging import init_logging
from tcp_dispatcher.utils.options import connector_types


def _print_logo():
    f = Figlet(font="slant")
    print("=" * 30)
    print(f.renderText("TCP Dispatcher"))
    print("=" * 30)


@click.group()
def cli():
    """
    CLI commands to run the TCP Dispatcher
    """


@cli.command()
@click.option(
    "--config_path",
    default=None,
    type=click.Path(exists=True),
    help="Path of custom configuration toml file",
)
def start(config_path: Optional[str]):
    """
    CLI command to start the tcp dispatcher
    """
    # read config
    conf = read_config(config_path=config_path)

    # Init logging
    init_logging(log_level=conf["log_level"])

    # print logo
    _print_logo()

    # Run TCP dispatcher
    tcp_dispatcher = TcpDispatcher(port=conf["dispatcher"]["dispatcher_port"], connectors_conf=conf["connectors"])
    tcp_dispatcher.run()


@cli.command()
@click.option(
    "--log_level",
    default="INFO",
    type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"]),
    prompt="What loglevel do you want to set?",
)
@click.option("--dispatcher_port", default=33577, prompt="What port should the dispatcher be set to?")
def init(log_level: str, dispatcher_port: int):
    """
    CLI command to init a folder with the correct structure to deploy a cryo_agent
    """

    # Get current working directory
    cwd = Path(os.getcwd())

    # Copy default_template.toml there and name if config.toml
    base_dir = Path(__file__).parent

    # Get template environment
    env = Environment(loader=PackageLoader("tcp_dispatcher", package_path="config"))

    # Set default template_kwargs
    filename = "config.toml"
    template_kwargs = {
        "filename": filename,
        "date": datetime.now().strftime("%d-%m-%Y"),
        "log_level": log_level,
        "dispatcher_port": dispatcher_port,
        "connectors": _conn_questions(),
    }

    # Render and write config
    rendered_template = env.get_template("config.tpl-toml").render(**template_kwargs)
    with open(cwd / Path(filename), "w", encoding="utf-8") as template_file:
        template_file.write(rendered_template)

    # Copy the start-up script
    shutil.copyfile(base_dir / "run/start_tcp_dispatcher_script.bat", cwd / Path("start_tcp_dispatcher_script.bat"))

    # Create log folder
    logs_path = cwd / "logs"
    if not os.path.exists(logs_path):
        os.mkdir(logs_path)


def _conn_questions() -> list[dict[str, Any]]:
    connector_configs = []
    while click.confirm("Do want to add (another) connector?", default=True):
        connector_type = click.prompt("What type of connector is needed?", type=click.Choice(connector_types))
        if connector_type == "COM":
            connector_configs.append(_com_questions())
        elif connector_type == "TCP":
            connector_configs.append(_tcp_questions())
        elif connector_type == "VISA":
            connector_configs.append(_visa_questions())
        elif connector_type == "GPIB":
            connector_configs.append(_gpib_question())
        else:
            raise Exception("Unknown connector type specified")

    return connector_configs


def _com_questions() -> dict[str, Any]:
    # Specify name
    name = click.prompt("Please specify the name of the external device", type=str)

    # Specify com port
    available_comports = [cp.device for cp in comports()]
    com_port = click.prompt(
        "Please specify the COM port the external device is connected to",
        type=click.Choice(available_comports) if len(available_comports) else str,
        default=available_comports[0] if len(available_comports) != 0 else "",
    )

    # Specify baudrate
    baudrate = click.prompt("Please specify the baud_rate to be used", type=int, default=9600)

    # Specify bytesize
    bytesize = click.prompt(
        "Please specify the bytesize to be used",
        type=click.Choice([str(opt) for opt in [serial.FIVEBITS, serial.SIXBITS, serial.SEVENBITS, serial.EIGHTBITS]]),
        default=str(serial.EIGHTBITS),
    )

    # Specify parity
    parity = click.prompt(
        "Please specify whether or not to use parity",
        type=click.Choice(
            [serial.PARITY_NONE, serial.PARITY_EVEN, serial.PARITY_ODD, serial.PARITY_MARK, serial.PARITY_SPACE]
        ),
        default=serial.PARITY_NONE,
    )

    # Specify stop bits
    stop_bits = click.prompt(
        "Please specify the number of stop bits to be used",
        type=click.Choice(
            [str(opt) for opt in [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO]]
        ),
        default=str(serial.STOPBITS_ONE),
    )

    # Specify line termination
    termination = click.prompt(
        "Please specify the line termination character",
        type=click.Choice(["CRLF", "CR", "LF"]),
        default="CRLF",
    )

    # Specify timeout
    timeout = click.prompt("Please specify the timeout in seconds to be used", type=int, default=5)

    # Return the dictionary of all answers
    return {
        "type": "COM",
        "name": name,
        "com_port": com_port,
        "baudrate": baudrate,
        "bytesize": bytesize,
        "parity": parity,
        "stop_bits": stop_bits,
        "termination": termination,
        "timeout": timeout,
    }


def _tcp_questions() -> dict[str, Any]:
    name = click.prompt("Please specify the name of the remote host", type=str)
    remote_host = click.prompt("Please specify the adress of the remote host", type=str)
    remote_port = click.prompt("Please specify the port of the remote host", type=int)
    return {"type": "TCP", "name": name, "remote_host": remote_host, "remote_port": remote_port}


def _visa_questions() -> dict[str, Any]:
    name = click.prompt("Please specify the name of the remote host", type=str)

    # Determine available addresses
    rm = visa.ResourceManager()
    available_instr = rm.list_resources()

    address = click.prompt("Please specify the address of the VISA instrument", type=click.Choice(available_instr))
    return {"type": "VISA", "name": name, "address": address}


def _gpib_question() -> dict[str, Any]:
    name = click.prompt("Please specify the name of the remote host", type=str)
    board_number = click.prompt(
        "Please specify the board number of the GPIB board (if applicable)", type=str, default=""
    )
    device_number = click.prompt("Please specify the GPIB device number", type=int)

    # Add board_number if provided
    init_dict = {"type": "GPIB", "name": name, "device_number": device_number}
    if board_number:
        init_dict.update({"board_number": int(board_number)})

    return init_dict
