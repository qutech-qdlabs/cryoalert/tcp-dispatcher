import logging
import selectors
import socket
from time import sleep
from types import SimpleNamespace

from tcp_dispatcher.router import Router
from tcp_dispatcher.utils.config import Config

LOGGER = logging.getLogger(__name__)


def _setup_socket(port: int):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("0.0.0.0", port))
    s.setblocking(False)
    return s


class TcpDispatcher:
    def __init__(self, port: int, connectors_conf: Config) -> None:
        self._sel = selectors.DefaultSelector()
        self.port = port
        self._socket = _setup_socket(port)

        # Register socket
        self._sel.register(self._socket, selectors.EVENT_READ, data=None)

        # Create remote client to which every message will be forwarded
        self._router = Router(connectors_conf)

    def accept_wrapper(self, sock: socket.socket):
        # Accept connection
        conn, addr = sock.accept()
        LOGGER.info(f"Accepted connection from {addr}")

        # Set connection to non-blocking mode, to allow for multiple active connections
        conn.setblocking(False)

        # Create data object for storing received and to-be-send data
        data = SimpleNamespace(addr=addr, inb=b"", outb=b"")

        # Register the socket with relevant events selectors
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self._sel.register(conn, events, data=data)

    def service_connection(self, key: selectors.SelectorKey, mask: int):
        # Extract socket and data from arguments
        sock: socket.socket = key.fileobj
        data = key.data

        # Process incoming events
        if mask & selectors.EVENT_READ:
            LOGGER.debug(f"Connection ready for reading")

            # Get data from incoming connection. No data means the connection was closed
            recv_data = sock.recv(1024)  # Should be ready to read
            if recv_data:
                LOGGER.debug(f"Forward to remote client: {recv_data!r}")
                data.outb += self._router.send(recv_data)
            else:
                LOGGER.info(f"Closing connection to {data.addr}")
                self._sel.unregister(sock)
                sock.close()

        if (mask & selectors.EVENT_WRITE) and data.outb:
            LOGGER.debug(f"Connection ready for writing")
            LOGGER.debug(f"Sending: {data.outb!r}")
            sent = sock.send(data.outb)  # Should be ready to write
            data.outb = data.outb[sent:]

    def loop(self):
        # Start connection to remote server
        self._router.connect()

        while True:
            # Wait for incoming event, then process it
            events = self._sel.select(timeout=1)
            if events:
                for key, mask in events:
                    if key.data is None:
                        self.accept_wrapper(key.fileobj)
                    else:
                        self.service_connection(key, mask)

    def run(self):
        # Start listening
        self._socket.listen()
        LOGGER.info(f"Listening on (0.0.0.0, {self.port})")

        while True:
            try:
                self.loop()
            except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError):
                # LOGGER.info(f"Could not connect to remote server located on {self.remote_host, self.remote_port}")
                sleep(1)
                self._router.disconnect()
            except KeyboardInterrupt:
                LOGGER.info("Closing TCP Dispatcher")
                self._sel.close()
                self._router.disconnect()
                break
