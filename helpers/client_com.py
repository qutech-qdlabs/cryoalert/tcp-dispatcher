import socket
from datetime import datetime

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 33577  # The port used by the server


def send_message(s: socket.socket, msg: str):
    print("Construct Message")
    msg = bytes(msg, encoding="utf-8")
    print(f"Send message: {msg}")
    s.sendall(msg)
    print("Read returned data")
    data = s.recv(1024)

    print(f"Received {data!r}")


# Try sending sequantially between the 2 sockets
s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(f"Connect to: ({HOST}, {PORT})")
s1.connect((HOST, PORT))
s2.connect((HOST, PORT))

send_message(s1, "@2R1")
send_message(s2, "@1R1")


s1.close()
s2.close()
