import socket

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    print(f"Started Socket at {PORT}")

    # Only needed on windows due to socket.accept not properly listening to Ctrl+C
    # s.settimeout(1)

    # Set host and port
    s.bind((HOST, PORT))

    try:
        s.listen()
        print("Awaiting new connection")
        conn, addr = s.accept()
        print(f"Connected by {addr}")
        while True:
            try:
                data = conn.recv(1024)
                if not data:
                    break
                print(f"Received data: {data}")
                conn.sendall(data)
                print("Returned data")
            except socket.timeout:
                print("Socket timed out")
            except Exception as exc:
                print(str(exc))
                print("Server closing")
                s.close()
                break
    except:
        print("Stopping echo socket server")
        s.close()
