import socket
from datetime import datetime

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 33577  # The port used by the server


def send_message(s: socket.socket, extra_msg: str):
    print("Construct Message")
    msg = bytes(f"Hello World at {datetime.now().isoformat()}: {extra_msg}", encoding="utf-8")
    print(f"Send message: {msg}")
    s.sendall(msg)
    print("Read returned data")
    data = s.recv(1024)

    print(f"Received {data!r}")


# Try sending sequantially between the 2 sockets
s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(f"Connect to: ({HOST}, {PORT})")
s1.connect((HOST, PORT))
s2.connect((HOST, PORT))
for _ in range(100):
    send_message(s1, extra_msg="Sending from s1")
    send_message(s2, extra_msg="Sending from s2")

# TODO try to send data from the second socket between sending and reading of the first socket
s1.sendall(b"Test from s1")
s2.sendall(b"Test from s2")
res1 = s1.recv(1024)
res2 = s2.recv(1024)
print(f"Received from s1: {res1!r}")
print(f"Received from s2: {res2!r}")
